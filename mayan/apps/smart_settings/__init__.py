from __future__ import unicode_literals

from .classes import Namespace, Setting  # NOQA

default_app_config = 'mayan.apps.smart_settings.apps.SmartSettingsApp'
